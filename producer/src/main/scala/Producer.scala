import scala.io.Source

object Producer extends App {
  val usage =
    """
      |Usage: producer <file name>
    """.stripMargin

  type OptionMap = Map[String, String]

  if (args.length == 0) {
    println(usage)
  } else {
    val arglist = args.toList

    def nextOption(map : OptionMap, list : List[String]) = {
      list match {
        case Nil => map
        case file :: Nil =>
          map ++ Map("file" -> file)
        case _ =>
          print(usage); map
      }
    }

    //Config to Map
    val options = nextOption(Map(), arglist)
    val fname = options.get("file")

    if (fname.isEmpty) {
      println(usage)
    } else {
        try {
          println(s"Read file ${fname.get}")

          val iteratorLine = Source.fromFile(fname.get).getLines()

          //for csv header skip
          iteratorLine.next()

          var cnt = 0
          iteratorLine.foreach { line =>
            //filtering option
//            val regex = raw"([\w-]+),(.+)".r
//            val regex = raw"([A-Za-z]+(-[A-Za-z0-9]+)),(.+)".r
//            val regex = raw"([\d-]+),([\w-]+),(\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2},\d{2}.\d{5}),(.+)".r

            line match {
              case data : String =>
                println(s"send data ${data} ")
                cnt += 1
                Kafka.send(Kafka.topicCsv, "mtrip", data)
//              case regex(topic, data) =>
//                println(s"send topic ${topic}, data ${data}")
//                cnt += 1
//                Kafka.send(Kafka.topicCsv, "key", data)
              case _ => println(s"Failed to match [${line}]")
            }
          }
        } catch {
          case e: Exception => println(s"Failed to read file ${fname}"); throw e
        }
    }
  }
}