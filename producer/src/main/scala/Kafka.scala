import ConfigLoader._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer

object Kafka {

  object kafkaParams {
    val producerOption = Map[String, Object](
      "bootstrap.servers" -> hosts,
      "key.serializer" -> classOf[StringSerializer].getName,
      "value.serializer" -> classOf[StringSerializer].getName,
      "group.id" -> "csv-producer-1",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )
  }

  val hosts = ConfigLoader.config.getOrElse[String]("producer.kafka.servers", "192.168.7.35:9095");
  val interval = ConfigLoader.config.getOrElse[Int]("producer.kafka.interval", 5)
  val maxRate = ConfigLoader.config.getOrElse[String]("producer.kafka.maxReate", "50")
  val topicCsv = ConfigLoader.config.getOrElse("producer.kafka.topics.csv", "topic-csv")

  println(s"hosts : ${ hosts}")
  println(s"interval : ${interval}")
  println(s"maxRate : ${maxRate}")
  println(s"topicCsv : ${topicCsv}")

  import scala.collection.JavaConverters._

  val producer = new KafkaProducer[String, String](Kafka.kafkaParams.producerOption.asJava)

  def send(topic: String, key: String, data: String) = {
    println(s"producer => ${producer}")
    producer.send(new ProducerRecord[String, String](topic, key, data))
  }

  def close() : Unit = {
    producer.close()
  }
}
