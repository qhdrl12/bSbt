//for universal plugins
addSbtPlugin("com.typesafe.sbt" %% "sbt-native-packager" % "1.2.2")
//for assembly plugins
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")