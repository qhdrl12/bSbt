import sbt._

object Dependencies {
  val SPARK_VERSION = "2.3.1"

  val joda        = "joda-time"  % "joda-time" % "2.8.1"
  val spark       = "org.apache.spark" %% "spark-core" % SPARK_VERSION
  val sparksql    = "org.apache.spark" %% "spark-sql" % SPARK_VERSION
  val cassandra   = "com.datastax.spark" %% "spark-cassandra-connector" % SPARK_VERSION

  val kafka       = "org.apache.spark" %% "spark-streaming-kafka-0-10" % SPARK_VERSION exclude("org.slf4j", "slf4j-log4j12")
  val config      = "com.typesafe" % "config" % "1.3.1"

  //test
  val scalatest        = "org.scalatest" %% "scalatest" % "3.0.5" % Test
  val cassandratest    = "com.datastax.spark" %% "spark-cassandra-connector-embedded" % SPARK_VERSION % Test
  val apachecassandra  = "org.apache.cassandra" % "cassandra-all" % "3.2 " % Test

  val basicDeps =
    Seq(joda, scalatest, config)
}
