import sbt.Keys.libraryDependencies
import NativePackagerHelper._

name := "bSbt"
version := "0.1"
scalaVersion := "2.11.12"

lazy val commonSettings = Seq(
  organization := "com.bSbt",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.11.12",

  libraryDependencies ++= Dependencies.basicDeps,

  scalaSource in Compile := baseDirectory(_ / "src/main/scala").value,
  resourceDirectory in Compile := baseDirectory(_ / "conf").value,
  scalaSource in Test := baseDirectory(_ / "src/test/scala").value,
  resourceDirectory in Test := baseDirectory(_ / "src/test/resources").value
)

lazy val common = project.in(file("common"))
    .settings(commonSettings)

lazy val converter = project.in(file("converter"))
    .enablePlugins(JavaAppPackaging)
    .settings(commonSettings,
      libraryDependencies += Dependencies.spark,
      libraryDependencies += Dependencies.sparksql,
      libraryDependencies += Dependencies.cassandra
//      mainClass in (Compile  packageBin) := Some("CsvReader") 
//      selectMainClass in (Compile  packageBin) := Some("CsvReader")
    )
    .dependsOn(common)

lazy val producer = project.in(file("producer"))
    .enablePlugins(JavaAppPackaging)
  .settings(commonSettings,
    libraryDependencies += Dependencies.kafka,
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
  ).dependsOn(common)

lazy val feature = project.in(file("feature"))
  .settings(commonSettings,
    resourceDirectory in Compile := baseDirectory(_ / "src/main/resources").value
  )

val root = (project in file("."))
  .enablePlugins(JavaAppPackaging, UniversalPlugin, UniversalDeployPlugin)
  .aggregate(converter)
  .aggregate(producer)
  .settings(
    aggregate in update := false,
    name in Universal := name.value,
    name in UniversalDocs := (name in Universal).value,
    name in UniversalSrc := (name in Universal).value,
    packageName in Universal := "bong-" ++ packageName.value
  )

TaskKey[Unit]("unzip") := {
  val args = Seq(baseDirectory.in(converter).value.getAbsolutePath + s"\\target\\universal\\converter-${(version in ThisBuild).value}")
  sys.process.Process("unzip", args) ! streams.value.log
}


