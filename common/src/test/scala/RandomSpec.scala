import org.scalatest.FunSuite

class RandomSpec extends FunSuite {
  test("RandomMatrix.rdInt") {
    val seq = Seq("a", "b", "c")

    val startInt = 1
    val endInt = 10
    val rdInt = RandomMatrix.randInt(startInt, endInt);
    assert(rdInt >= startInt && rdInt <= endInt)

    val startFloat = 1.1f
    val endFloat = 1.5f
    val rdFloat = RandomMatrix.randFloat(startFloat, endFloat);
    assert(rdFloat > startFloat && rdFloat <= endFloat)

    assert(seq.contains(RandomMatrix.randArray(seq, "o")))
  }
}
