object MandatoryParams {
  def valid(params: Map[String, String], mandatory: Seq[String]) = {
    var b = true

    import scala.util.control.Breaks._

    breakable {
      mandatory.foreach(m => {
        if (params(m) == null)
          b = false; break
      })
    }
    b
  }
}
