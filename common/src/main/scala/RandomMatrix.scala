
class RandomMatrix {

}

object RandomMatrix extends App {

  def randInt(start: Int, end: Int)= {
    val rnd = new scala.util.Random
    start + rnd.nextInt( (end - start) + 1)
  }

  def randFloat(start: Float, end: Float) = {
    val rnd = new scala.util.Random
    start + rnd.nextFloat() * (end - start)
  }

  def randArray(arr: Seq[Any], default: String) = {
    arr.length match {
      case 0 =>
        default
      case 1 =>
        arr.apply(0).toString
      case _ =>
        val idx = randInt(0, arr.length-1)
        arr.apply(idx).toString
    }
  }

  var arr = Seq("red", "blue", "yello", "black")

  for (i <- 1 to 99) {
    println(s"${i} => ${randInt(20, 30)}")
    println(s"${i} => ${randFloat(10.0001f, 20.1000f)}")
    println(s"${i} => ${randArray(arr, "imsi")}")
  }

}