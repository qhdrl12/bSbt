import com.typesafe.config._

object ConfigLoader {

  type Getter[T] = (Config, String) => T

  implicit val stringGetter: Getter[String]           = _ getString _
  implicit val booleanGetter: Getter[Boolean]         = _ getBoolean _
  implicit val intGetter: Getter[Int]                 = _ getInt _
  implicit val doubleGetter: Getter[Double]           = _ getDouble _


  implicit class ConfigOps(val config: Config) extends AnyVal {
    def getOrElse[T : Getter](path: String, defValue: => T): T = opt[T](path) getOrElse defValue
    def opt[T : Getter](path: String): Option[T] = {
      if (config hasPathOrNull path) {
        val getter = implicitly[Getter[T]]
        Some(getter(config, path))
      } else {
        None
      }
    }
  }

  lazy val config: Config = ConfigFactory.load()
}
