import CsvReader.OptionMap
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat

case class KafkaTrip(vrn: String, ttime: String, nick: Option[String], lat: Double, lon: Double) extends  CsvInterface {

  def toDateTime: String = {
    val dateArray = ttime.split("\\s+")
    val timeRegex = """([0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3})"""
    val TimeMills = timeRegex.r

    if (dateArray.length == 2) {
      val ymd = dateArray(0).replaceAll("/", "-")
      val time = dateArray(1) match {
        case TimeMills(t) => t
        case other => other + ".000"
      }

      ymd + "T" + time
    } else {
      ttime
    }
  }

  def toTimestamp: Long = {
    try {
      DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS").parseDateTime(toDateTime).getMillis
    } catch {
      case e: Exception => println(s"exception : $e"); 0
    }
  }

  override def isEnoughParams(optionMap: OptionMap): Boolean = {
    true
  }

  override def toPayload = {
    s"""{
       |"lat":$lat,
       |"lon":$lon,
       |"clt":$toTimestamp,
       |"sp": ${RandomMatrix.randInt(0,60)}
       |}
    """.stripMargin.replaceAllLiterally("\\n", "").trim
  }

  override def toString: String = getClass.getName +
                                  s"""
                                    |{
                                    | vrn : "$vrn",
                                    | ttime : "$toDateTime",
                                    | nick: "${nick.get}",
                                    | lat : $lat,
                                    | lon : $lon,
                                    | ts : $toTimestamp
                                    |}
                                  """.stripMargin
}

object KafkaTrip {

  def apply(optionMap: OptionMap) = {
//      val tripVariable = tempTrip.getClass.getDeclaredFields.map(_.getName)

      new KafkaTrip(
        optionMap.getOrElse("vrn", "1915"),
        optionMap.getOrElse("ttime", LocalDateTime.now.toString),
        Some(optionMap.getOrElse("nick", "")),
        optionMap.getOrElse("lat", "37.123456").toDouble,
        optionMap.getOrElse("lon", "127.123456").toDouble
      )
  }

}