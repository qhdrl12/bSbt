import scala.io.Source

object CsvReader extends App {

    private val useage =
        """Usage: excel-data-converter [options] <file_name>
          | -c <col1,col2>  : column list
          | -s              : show 1 line with header
        """.stripMargin

    type OptionMap = Map[String, String]

    if (args.length == 0) {
        println(useage)
    } else {
        def nextOption(map: OptionMap, list: List[String]): OptionMap = {
            list match {
                case Nil => map
                case ("-s") :: tail =>
                    nextOption(map ++ Map("show" -> ""), tail)
                case ("-c") :: value :: tail =>
                    nextOption(map ++ Map("col" -> value), tail)
                case file :: Nil =>
                    map ++ Map("input" -> file)
                case _ =>
                    Map("invalid" -> "")
            }
        }

        val options = nextOption(Map(), args.toList)

        if (options.contains("invalid")) {
            println(useage)
            System.exit(1)
        }

        parseCsvFile(options, kafkaTripByLine)

    }

    def kafkaTripByLine(optionMap: OptionMap): Unit = {
        val kafkaTrip = KafkaTrip(optionMap)
        println(s"$kafkaTrip")
//        println(s"${kafkaTrip.toPayload}")
    }

    def parseCsvFile(options: OptionMap, callback: OptionMap => Unit): Unit = {

        def splitColsWithHeader(line: String, header: String): (Array[String], Array[String]) = {
            (line.split(","), header.split(","))
        }

        def show(pCol: (Array[String], Array[String])): Unit = {
            val cols = pCol._1
            val headers = pCol._2

            for (((h, c),i) <- headers zip cols zipWithIndex) {
                println(s"[$i] $h : $c")
            }
        }

        def parse(pCol: (Array[String], Array[String])) : OptionMap = {
            val definedCols = options("col").split(",") //todo condition add
            val cols = pCol._1
            val headers = pCol._2

            val temp = cols.zip(headers).zipWithIndex.filter(c => definedCols.contains(c._2.toString)).map(c => {
                // println(s"${c._1._2} => ${c._1._1}")
                c._1._2 -> c._1._1
            }).toMap

            temp
        }

        /**
          * Start
          */
        val filename = options("input")
        if (filename.isEmpty) {
            println("File name not found.")
            println(useage)
        } else {
            try {
                val file  = Source.fromFile(options("input"), "UTF-8").getLines()
//                val file  = Source.fromResource(options("input")).getLines()
                val header = file.next()

                if (options.contains("show")) {
                    show(splitColsWithHeader(file.next(), header))
                } else {
                    for (line <- file) {
                        val transferMap = parse(splitColsWithHeader(line, header))
                        // println(transferMap)

                        //todo process business logic
                        callback(transferMap)
                    }
                }
            } catch {
                case e: Exception =>
                    println(s"Failed to read file $filename : $e")
            }
        }
    }
}
