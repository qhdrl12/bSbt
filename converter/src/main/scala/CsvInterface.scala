
trait CsvInterface {
  def isEnoughParams(m : Map[String, String]): Boolean
  def toPayload: String
}
