import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector._
import org.apache.spark.{SparkConf, SparkContext}

class SparkMain {
    val conf = new SparkConf().setMaster("local[*]").setAppName("sparkTest")
      .set("spark.cassandra.connection.host", "dev1")
      .set("spark.cassandra.auth.username", "cassandra")
      .set("spark.cassandra.auth.password", "cassandra123o")

  val sc = new SparkContext(conf)
  val connector = CassandraConnector(sc.getConf)
}

object SparkMain extends App {
  val sc = (new SparkMain).sc

  val rdd = sc.cassandraTable("test", "words")

  rdd.foreach(println)
}
