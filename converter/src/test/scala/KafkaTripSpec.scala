import org.scalatest.FunSuite

class KafkaTripSpec extends FunSuite {
  test("KafkaTrip.toDate") {
    val trip = KafkaTrip("1516", "2018-07-04 14:00:05", Some("bong"), 37.232, 126.1321)
    println(trip)

    val param = trip.getClass.getDeclaredFields.map(_.getName)
    param.foreach(println)

  }
}