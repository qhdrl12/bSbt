const dateFormat = require('dateformat');

let now = new Date();

const generatorRandomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
}

const generatorDatetime = (format, sec = 0) => {
    let datetime = (sec == 0) ? dateFormat(new Date(), format) :
        dateFormat(now.setSeconds(now.getSeconds() + sec), format);

    return datetime;
}

module.exports.generatorRandomNumber = generatorRandomNumber;
module.exports.generatorDatetime = generatorDatetime;