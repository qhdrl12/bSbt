package course

class convariant[+T]
// +T : 상위 = 하위
// -T : 하위 = 상위
//  T : 동일
object capter10 extends App {

  val cv1: convariant[String] = new convariant[String] //AnyRef
  val cv2: convariant[AnyRef] = new convariant[String]
}
