package course

class capter11

object capter11 extends App {
  val a = new String("hello")
  val b = new String("hello")
  val c = b

  // == 값비교
  println(s"${a == b}")

  // eq 객체 비교
  println(s"${a eq b}")
}
