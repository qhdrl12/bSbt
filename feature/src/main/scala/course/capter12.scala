package course

class capter12 {
  private var sum = 0
  var name = "default"

  def this(name: String) {
    this()
    this.name = name
  }

  def this(sum: Int) {
    this()
    this.sum = sum
  }

  override def toString: String = getClass.getName + s"sum=$sum,name=$name"
}

object capter12 extends App {
  val a = new capter12(2)
  val b = new capter12("bong")

  println(a.sum)
  println(a)
  println(a.hashCode())

  println(b.name)
  println(b)
  println(b.hashCode())

  val p = new Person("bong", 20)

  println(s"${p.name}, ${p.age}")

}

case class Person(name: String, age: Int)
