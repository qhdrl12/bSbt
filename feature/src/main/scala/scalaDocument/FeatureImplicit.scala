package scalaDocument

class Feature {
}

abstract class SemiGroup[T] {
  def add(x: T, y: T) : T
}

abstract class Monoid[T] extends SemiGroup[T] {
  def unit: T
}

object Feature extends App {
  implicit object StringMonoid extends Monoid[String] {
    def add(x: String, y: String): String = x concat y
    def unit: String = ""
  }

  implicit object IntMonoid extends Monoid[Int] {
    def add(x: Int, y: Int): Int = x+y
    def unit: Int = 0
  }

  def sum[T] (xs: List[T]) (implicit m: Monoid[T]): T =
    if (xs.isEmpty) m.unit
    else {
      println(s"${xs.head}, ${xs.tail}")
      m.add(xs.head, sum(xs.tail))
    }

  println(sum(List(1,2,3)))
  println(sum(List("a","b","c")))
}