package scalaDocument

class FeatureCaseClass {

}

abstract class Notification

case class Email(sourceEmail: String, title: String, body: String) extends Notification

case class SMS(sourceNumber: String, message: String) extends Notification

case class VoiceRecording(contactName: String, link: String) extends Notification

object FeatureCaseClass extends App {

  def showNotification(notification: Notification) = {
    notification match {
      case Email(email, title, body) =>
        "You got and email from " + email + " with title : " + title
      case SMS(number, message) =>
        "You got an SMS from " + number + "! Message: " + message
      case VoiceRecording(name, link) =>
        "you recevied a Voice Recording from " + name + "! Click the link to hear it: " + link
    }
  }

  def matchTest(x: Any): Any = {
    x match {
      case 1 => "one"
      case "two" => 2
      case y: Int => s"scala.Int $y"
      case x: String => s"java.lang.String $x"
      case _ => s"invalid input"
    }
  }

  val emailFromBong = Email("qhdrl12@naver.com", "hello Bong", "BangapSupneda")
  println(emailFromBong.sourceEmail)

  val editEmail = emailFromBong.copy(title = "edit title!")
  println(editEmail.title)

  println(showNotification(emailFromBong))

  println(matchTest("one"))
}
