package scalaDocument

class Abstract {

}

trait Buffer[+T] {
  val element: T
}

abstract class SeqBuffer[U, +T <: Seq[U]] extends Buffer[T] {
  def length = element.length
}

object AbstractTest extends App {
  def newIntSeqBuf(e1: Int, e2: Int) =
    new SeqBuffer[Int, List[Int]] {
      override val element: List[Int] = List(e1, e2)
    }

  def newStringSeqBuf(e1: String, e2: String) =
    new SeqBuffer[String, List[String]] {
      override val element: List[String] = List(e1, e2)
    }

  val buf = newIntSeqBuf(2, 8)
  println(s"length =  ${buf.length}")
  println(s"content = ${buf.element}")

  val sbuf = newStringSeqBuf("bong", "hello")
  println(s"length =  ${sbuf.length}")
  println(s"content = ${sbuf.element}")
}
