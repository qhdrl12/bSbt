package scalaDocument

trait Cloneable extends java.lang.Cloneable {
  override def clone() ={
    super.clone().asInstanceOf[Cloneable]
  }
}

trait Resetable {
  def reset: Unit
}

//def cloneAndRest(obj: Cloneable with Resetable) : Cloneable = {
//  val cloned = obj.clone
//  obj.reset
//  cloned
//}

trait Start[T] {
  def doStart: T
}

trait Stop[T] {
  def doStop: T
}

abstract class Car[T] extends Start[T] with Stop[T]

object CloneableTest extends App {
  implicit object StringCar extends Car[String] {
    override def doStop = "Brrrrr!!"
    override def doStart = "......."
  }

  implicit object IntCar extends Car[Int] {
    override def doStop = 99
    override def doStart = 0
  }

  def callCar[T](xs: T) (implicit c: Car[T]) = {
    (c.doStart, c.doStop)
  }

  println(callCar("bcar"))
  println(callCar(99))
}
