package scalaDocument

abstract class Graph {
  type Edge
  type Node <: NodeInf

  abstract class NodeInf {
    def connectWith(node: Node)
  }

  def nodes: List[Node]
  def edges: List[Edge]
  def addNode: Node
}

abstract class DirectedGraph extends Graph {
  override type Edge <: EdgeImpl

  class EdgeImpl(origin: Node, dest: Node) {
    def from = origin
    def to = dest
  }

  class NodeImpl extends NodeInf {
    override def connectWith(node: Node): Unit = {

    }
  }

}
