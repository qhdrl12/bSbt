package scalaDocument

class FeatureUnapply {

}

object Twich {
  def apply(x: Int) = x * 3
  def unapply(z: Int): Option[Int] = {
    if (z%2 == 0) Some(z/2) else None
  }
}

object FeatureUnapply extends App {
  val x = Twich(21)
  x match {
    case Twich(n) => Console.println(n)
    case _ => Console.print("홀수 입니다.")
  }
}
