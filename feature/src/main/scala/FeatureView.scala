class FeatureView[A <% Int] {
  def addlt(x: A) = 123 + x
}

object FeatureView extends App {

  implicit def strToTInt(x: String) = x.toInt


  val x = "123"

  println(s"result = ${x}")

  val y: Int = "125"

  println(s"result = ${x}")

  val m = math.max(x, y)

  println(s"math $m")

  val fa = new FeatureView[String].addlt(x)

  println(s"feature addlt = ${fa}")

  val fi = new FeatureView[Int].addlt(y)

  println(s"feature addlt = ${fi}")

  val ls = List(2).sum

  println(s"list sum ${ls}")
}
